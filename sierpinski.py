bl_info = {
    "name": "Sierpinski tetrahedron",
    "description": "Sierpinski tetrahedron",
    "author": "David Mladek",
    "version": (1, 0, 0),
    "blender": (2, 77, 0),
    "location": "View3D > Add > Mesh > Sierpinski",
    "warning": "",
    "wiki_url": "https://edux.fit.cvut.cz/courses/BI-PGA/student/mladedav/dokumentace2",
    "category": "Add Mesh"
}

import bpy
from mathutils import *
from math import *
from bpy.props import *

def create_mesh_object(context, verts, edges, faces, name):
    mesh = bpy.data.meshes.new(name)
    mesh.from_pydata(verts, edges, faces)
    mesh.update()

    from bpy_extras import object_utils
    return object_utils.object_data_add(context, mesh, operator=None)

class Sierpinski_primitive:
    def __init__(self, position, size, level):
        self.cnt = 0
        self.verts = []
        self.edges = []
        self.faces = []
        self.add_sierpinski ( position, size, level )

    def add_tetra ( self, vertices ):
        for v in vertices:
            self.verts.append(tuple(v.values()))
            
        self.faces.append([4 * self.cnt + 1, 4 * self.cnt + 2, 4 * self.cnt + 3])
        self.faces.append([4 * self.cnt + 0, 4 * self.cnt + 1, 4 * self.cnt + 3])
        self.faces.append([4 * self.cnt + 0, 4 * self.cnt + 3, 4 * self.cnt + 2])
        self.faces.append([4 * self.cnt + 0, 4 * self.cnt + 2, 4 * self.cnt + 1])
        self.cnt += 1

    def compute_vertices ( position, size ):
        return (
            {
                'x' : position['x'],
                'y' : position['y'],
                'z' : position['z'] + size * sqrt ( 2 ) / sqrt ( 3 )
            },
            {
                'x' : position['x'],
                'y' : position['y'] + size / sqrt ( 3 ),
                'z' : position['z']
            },
            {
                'x' : position['x'] + size / 2,
                'y' : position['y'] - size / 2 / sqrt ( 3 ),
                'z' : position['z']
            },
            {
                'x' : position['x'] - size / 2,
                'y' : position['y'] - size / 2 / sqrt ( 3 ),
                'z' : position['z']
            }
        )

    def compute_positions ( position, size ):
        return Sierpinski_primitive.compute_vertices ( position, size / 2 )

    def add_sierpinski ( self, position, size, level ):
        if level == 1:
            self.add_tetra ( Sierpinski_primitive.compute_vertices ( position, size ) )
            return

        for pos in Sierpinski_primitive.compute_positions ( position, size ):
            self.add_sierpinski ( pos, size / 2, level - 1 )

class Sierpinski(bpy.types.Operator):
    """Sierpinski tetrahedron"""
    bl_idname = "object.sierpinski"
    bl_label = "Sierpinski tetrahedron"
    bl_options = {'REGISTER', 'UNDO'}
    
    level = bpy.props.IntProperty(
        name = 'Level', 
        description = 'Level of recursion',
        default = 3, min = 1, max=15, step=1
    )
    
    size = bpy.props.FloatProperty(
        name = 'Size',
        description = 'Size',
        default = 1
    )
    
    def execute(self, context):
        # vytvorení geometrie
        data = Sierpinski_primitive( { 'x':0, 'y':0, 'z':0 }, self.size, self.level )

        # vytvorení objektu (meshe)
        obj = create_mesh_object(context, data.verts, data.edges, data.faces, "Sierpinski")
        
        # get rid of double vertices
        bpy.context.scene.objects.active = obj.object
        bpy.ops.object.mode_set(mode='EDIT', toggle=False)
        bpy.ops.mesh.remove_doubles()
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    
        return {'FINISHED'}
    
def menu_func(self, context):
    self.layout.operator(Sierpinski.bl_idname)

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_mesh_add.append(menu_func)
def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_mesh_add.remove(menu_func)

if __name__ == "__main__":
    register()
